package br.com.instituicao.escola;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class EscolaService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private EscolaRepository escolaRepository;

	public List<EscolaDTO> findAll() {
		return escolaRepository.findAll().stream().map(EscolaDTO::create).collect(Collectors.toList());
	}

	public EscolaDTO findById(Long id) throws ObjectNotFoundException {
		Optional<Escola> escola = escolaRepository.findById(id);
		return escola.map(EscolaDTO::create).orElseThrow(() -> new ObjectNotFoundException("Escola não encontrada!"));
	}

	public EscolaDTO save(Escola escola) {
		Assert.isNull(escola.getId(), "Não foi possível inserir o registro.");
		return EscolaDTO.create(escolaRepository.save(escola));
	}

	public EscolaDTO update(Escola escola, Long id) {
		Assert.isNull(id, "Não foi possível atualizar o registro.");
		Optional<Escola> optional = escolaRepository.findById(id);
		if (optional.isPresent()) {
			Escola update = optional.get();
			update.setNome(escola.getNome());
			update.setEndereco(escola.getEndereco());
			save(update);
			return EscolaDTO.create(update);
		}
		return null;
	}

	public void deleteById(Long id) {
		escolaRepository.deleteById(id);
	}

	public void deleteAll() {
		escolaRepository.deleteAll();
	}

}
