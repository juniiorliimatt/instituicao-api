package br.com.instituicao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstituicaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstituicaoApiApplication.class, args);
	}

}
