package br.com.instituicao.aluno;

import java.io.Serializable;

public class AlunoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String nome;
	private String sobrenome;
	private long matricula;
	private String curso;

	public AlunoDTO() {
	}

	public AlunoDTO(Aluno aluno) {
		this.id = aluno.getId();
		this.nome = aluno.getNome();
		this.sobrenome = aluno.getSobrenome();
		this.matricula = aluno.getMatricula();
		this.curso = aluno.getCurso();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public long getMatricula() {
		return matricula;
	}

	public void setMatricula(long matricula) {
		this.matricula = matricula;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

}
