package br.com.instituicao.aluno;

import java.io.Serializable;
import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/aluno")
public class AlunoController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private AlunoService service;

	@GetMapping("/")
	public ResponseEntity<List<AlunoDTO>> findAll() {
		try {
			List<AlunoDTO> alunos = service.findAll();
			if (alunos.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok().body(alunos);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping
	public ResponseEntity<AlunoDTO> save(@RequestBody AlunoDTO dto) {
		try {
			dto = service.save(dto);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId())
					.toUri();
			return ResponseEntity.created(uri).body(dto);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
}
